<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 5/4/17
  Time: 3:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Student Input</title>
</head>
<body>
    <form action="input-student" method="post">
        <label for="age">Age</label>
        <input type="text" id="age" name="age">

        <label for="name">Name</label>
        <input type="text" id="name" name="name">

        <label for="id">Id</label>
        <input type="text" id="id" name="id">
        <br>
        <select name="skills" id="">
            <options value="math">Math</options>
            <option value="Bio">Biology</option>
            <option value="archaeology">Archaeology</option>
        </select>
        <br>
        <label for="city">City</label>
        <input type="text" id="city" name="addresss.city">
        <br>

        <label for="country">country</label>
        <input type="text" id="country" name="addresss.country">
        <br>

        <label for="street">street</label>
        <input type="text" id="street" name="addresss.street">
        <br>


        <input type="submit" value="Submit">
    </form>
</body>
</html>
