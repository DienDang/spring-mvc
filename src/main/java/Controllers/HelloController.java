package Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by dangdien on 5/4/17.
 */
@Controller
public class HelloController {
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String Hello(ModelMap modelMap) {
        modelMap.addAttribute("message", "Hello Spring MVC");
        return "hello";
    }
}
