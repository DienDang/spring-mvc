package Controllers;

import com.sun.org.apache.regexp.internal.RE;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

/**
 * Created by dangdien on 5/4/17.
 */
@Controller
public class StudentInputController {
    @InitBinder
    public void initBinder(WebDataBinder webDataBinder){
    }

    @RequestMapping(value = "input", method = RequestMethod.GET)
    public String inputStudent(){
        return "Student/StudentForm";
    }


    @RequestMapping(value = "input-student", method = RequestMethod.POST)
    public String viewString(@ModelAttribute("student") Student student, ModelMap modelMap, HttpServletRequest request, @RequestParam("name") String name, BindingResult result) {
        if(result.hasErrors()){
            return "Student/StudentForm";
        }
        modelMap.addAttribute("studentObject", student);
                return "Student/student-view";
    }
}
