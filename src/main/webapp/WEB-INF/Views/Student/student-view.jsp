<%--
  Created by IntelliJ IDEA.
  User: dangdien
  Date: 5/4/17
  Time: 4:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isELIgnored="false" %>
<html>
<head>
    <title>Output</title>
</head>
<body>
    <h3>Name: </h3>
    <h5>${studentObject.name}</h5>
    <br>
    <h3>Age: </h3>
    <h5>${age}</h5>
    <br>
    <h3>ID: </h3>
    <h5>${id}</h5>
    <br>
    <h6>${studentObject.skills}</h6>
    <br>
    <h3>street: </h3>
    <h5>${studentObject.addresss.street}</h5>
    <br>
    <h3>city: </h3>
    <h5>${studentObject.addresss.city}</h5>
    <br>
    <h3>country: </h3>
    <h5>${studentObject.addresss.country}</h5>

</body>
</html>
