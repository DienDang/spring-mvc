package Controllers;

import java.util.List;

/**
 * Created by dangdien on 5/4/17.
 */
public class Student {
    private Integer age;
    private String name;
    private Integer id;
    private List<String> skills;
    private Address addresss;

    public Student(Integer age, String name, Integer id, List<String> skills, Address address) {
        this.age = age;
        this.name = name;
        this.id = id;
        this.skills = skills;
        this.addresss = address;
    }

    public Address getAddresss() {
        return addresss;
    }

    public void setAddresss(Address addresss) {
        this.addresss = addresss;
    }

    public Student() {
    }

    public List<String> getSkills() {
        return skills;
    }

    public void setSkills(List<String> skills) {
        this.skills = skills;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Student{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", id=" + id +
                ", skills=" + skills +
                '}';
    }
}
